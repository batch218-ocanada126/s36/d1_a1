/*
	Gitbash:
		npm init -y
		npm i express
		npm i mongoose
		touch .gitignore >> content: node_modules
*/

// dependencies
const express = require("express");
const mongoose = require("mongoose");

const taskRoute = require("./routes/taskRoute.js");

// Server Setup

const app = express();
const port = 3001;

//Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// DB connection
mongoose.connect("mongodb+srv://admin:admin@b218-to-do.eghwkgw.mongodb.net/toDo?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

app.use("/tasks", taskRoute);

app.listen(port, ()=> console.log(`Now listening to port ${port}`));


// Gitbash:
// Start node application:
// nodemon index.js