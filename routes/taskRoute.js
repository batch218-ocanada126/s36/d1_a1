// This document contains all the endpoints for our application (also http methods)

const express = require("express");
const router = express.Router();

const taskController = require("../controllers/taskController.js");

router.get("/viewTasks", (req, res) => {

    taskController.getAllTasks().then(result => res.send(result));
})

router.post("/addNewTask", (req, res)=>  {
	taskController.createTask(req.body).then(result => res.send(result));
});

					// :id will serve as our wildcard
router.delete("/deleteTask/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(result => res.send(result));
})


router.put("/updateTask/:id", (req, res) => {
		//req.params.id - will be the basis of what documents we will update
		//req.body - the new documents/contents 
	taskController.updateTask(req.params.id, req.body).then(result => res.send(result));
})


router.get("/:id", (req, res) => {
	taskController.getSpecificTask(req.params.id).then(result => res.send(result));
})


router.put("/:id/complete", (req, res) => {
	taskController.updateStatus(req.params.id, req.body).then(result => res.send(result));
})

module.exports = router;

