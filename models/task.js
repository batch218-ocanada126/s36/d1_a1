// This documents contain Schema


const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({
	name: {
		type: String,
		require: [true, "Task name is required"]
	},
	status: {
		type: String,
		default: "pending"
	}
})

module.exports = mongoose.model("Task", taskSchema);