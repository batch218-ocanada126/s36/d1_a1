// This document contains our app feature in retrieve displaying and manipulating our database

const Task = require("../models/task.js");
	
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	})
}

module.exports.createTask = (requestBOdy) => {
	let newTask = new Task({
		name : requestBOdy.name
	})

	return newTask.save().then((task, error) => {
		if(error){
			console.log(error);
			return false; //"Error detected"
		}
		else{
			return task;
		}
	})
}

// "taskId" parameter will serve as storage of id from our url/link
module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {

		if(err){
			console.log(err);
			return false;
		}
		else{
			return removedTask;
		}
	})
}


module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {

		if(error){
			console.log(error)
			return false;
		}

		result.name = newContent.name;

		return result.save().then((updateTask, saveErr) =>{
			if(saveErr){
				console.log(saveErr);
				return false
			}
			else{
				return updateTask;
			}
		})
	})
}

/*
[wildcard required]
1. Create a controller function for retrieving a specific task.
2. Create a route 
3. Return the result back to the client/Postman.
4. Process a GET request at the "/tasks/:id" route using postman to get a specific task.

[PUT]
5. Create a controller function for changing the status of a task to "complete".
6. Create a route
7. Return the result back to the client/Postman.
8. Process a PUT request at the "/tasks/:id/complete" route using postman to update a task.

9. Create a git repository named S31.
10. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
11. Add the link in Boodle.

*/

module.exports.getSpecificTask = (taskId) => {
	return Task.findById(taskId).then(result => {
		return result;
	})
}


module.exports.updateStatus = (statusId) => {
	return Task.findByIdAndUpdate(statusId).then((result, error) => {

		if(error){
			console.log(error)
			return false;
		}

		result.status = "complete";

		return result.save().then((updateStatus, saveErr) =>{
			if(saveErr){
				console.log(saveErr);
				return false
			}
			else{
				return updateStatus;
			}
		})
	})
}
